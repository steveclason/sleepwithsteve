'use strict';

var gulp = require('gulp');
var sass = require( 'gulp-sass' );
var sourcemaps = require('gulp-sourcemaps');
// var rename = require( 'gulp-rename' );
// var minify = require( 'gulp-minify-css' );
var autoprefixer = require( 'gulp-autoprefixer' );
var util = require( 'gulp-util' );
var log = util.log;
//require( 'stylelint' )(),

gulp.task('default', function() {
  // place code for your default task here
});

gulp.task('postcss', function() {
  // place code for your default task here
});

gulp.task('sass', function () {
  log( 'Generate CSS files ' + (new Date()).toString());
  return gulp
    .src('./sleepwithsteve/sass/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe( autoprefixer( 'last 4 version' ))
    .pipe( sourcemaps.write('.'))
    .pipe( gulp.dest( './sleepwithsteve' ));

  // .pipe( rename({ suffix: '.min' }))
  // .pipe(minify())
  // .pipe(gulp.dest( './inn97win' ));
});

// gulp.task('sass:watch', function () {
//   gulp.watch('./inn97win/sass/**/*.scss', ['sass']);
// });
