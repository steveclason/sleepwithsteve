# README #

SleepWithSteve is a WordPress starter theme, itself started from \_s (Underscores), a starter theme from Automattic. There is minimal layout done and only enough element definition (colors, borders, etc.) necessary for making the main elements visible. The layout depends on newer CSS features, like Grid and Flexbox, so would need some fallbacks to be used in a production environment, probably. It's viewport-responsive and the JavaScript depends on jQuery which is always, for now, available in WordPress so doesn't add any performance burden.

### What is this repository for? ###

This theme is for theme developers.

The theme came out of my desire to have a well-developed starter theme specifically for hotels/ resorts. I've done quite a few of those, enough to have some idea of what is needed, and the effort here will simplify and speed-up the development of client-specific sites.

There are built-in custom post types for Rooms, Packages, Amenities, and Testimonials, and the layouts of the archive pages for those CPTs are stubbed in. There's a client-editable stylesheet, style2.css, which doesn't need to be compiled from SCSS files so can be used by less knowledgable developers to make style changes.

The intent is not to provide a finished theme, just to save some time during development.

### Features ###
* I'm trying to do this in little, self-contained modules so they can easily be selected and de-selected for use, depending on the site requirements.
* Mobile first.

### Things To Do ###
* add a Call-To-Action shortcode
* include Flexslider?

### How do I get set up? ###

To use the theme, you'll need:
* pretty good knowledge of HTML, CSS, PHP, JavaScript, and the WordPress environment -- this isn't a click-the-boxes theme;
* comfort using SASS to create CSS;
* a graphic design (this isn't it);

* nodding familiarity with Node, Gulp, and a few other things;
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
