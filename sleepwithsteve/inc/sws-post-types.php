<?php
/**
 * Custom post types for sleepwithsteveeloutlook.com
 */
function create_sws_post_types() {

  /*
   * Testimonials have a featured image, an excerpt used as a pull quote and testimonial content.
   */
  $labels = array(
    'name'               => _x( 'Testimonials', 'post type general name', 'sleepwithsteve' ),
    'singular_name'      => _x( 'Testimonial', 'post type singular name', 'sleepwithsteve' ),
    'menu_name'          => _x( 'Testimonials', 'admin menu', 'sleepwithsteve' ),
    'name_admin_bar'     => _x( 'Testimonial', 'add new on admin bar', 'sleepwithsteve' ),
    'add_new'            => _x( 'Add New', 'Testimonial', 'sleepwithsteve' ),
    'add_new_item'       => __( 'Add New Testimonial', 'sleepwithsteve' ),
    'new_item'           => __( 'New Testimonial', 'sleepwithsteve' ),
    'edit_item'          => __( 'Edit Testimonial', 'sleepwithsteve' ),
    'view_item'          => __( 'View Testimonial', 'sleepwithsteve' ),
    'all_items'          => __( 'All Testimonials', 'sleepwithsteve' ),
    'search_items'       => __( 'Search Testimonials', 'sleepwithsteve' ),
    'parent_item_colon'  => __( 'Parent Testimonials:', 'sleepwithsteve' ),
    'not_found'          => __( 'No Testimonials found.', 'sleepwithsteve' ),
    'not_found_in_trash' => __( 'No Testimonials found in Trash.', 'sleepwithsteve' )
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'testimonials-archive' ),
    'capability_type'    => 'page',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_icon'					 => 'dashicons-thumbs-up',
    'menu_position'			=> 20,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'custom-fields' )
  );
  register_post_type( 'sws_testimonials', $args );

  // TODO: register Rooms post-type.
  /*
   * Rooms have a featured image, an excerpt used as a pull quote and content. There is a custom
   * taxonomy associated with the post-type.
   */
  $labels = array(
    'name'               => _x( 'Rooms', 'post type general name', 'sleepwithsteve' ),
    'singular_name'      => _x( 'Room', 'post type singular name', 'sleepwithsteve' ),
    'menu_name'          => _x( 'Rooms', 'admin menu', 'sleepwithsteve' ),
    'name_admin_bar'     => _x( 'Room', 'add new on admin bar', 'sleepwithsteve' ),
    'add_new'            => _x( 'Add New', 'Room', 'sleepwithsteve' ),
    'add_new_item'       => __( 'Add New Room', 'sleepwithsteve' ),
    'new_item'           => __( 'New Room', 'sleepwithsteve' ),
    'edit_item'          => __( 'Edit Room', 'sleepwithsteve' ),
    'view_item'          => __( 'View Room', 'sleepwithsteve' ),
    'all_items'          => __( 'All Rooms', 'sleepwithsteve' ),
    'search_items'       => __( 'Search Rooms', 'sleepwithsteve' ),
    'parent_item_colon'  => __( 'Parent Rooms:', 'sleepwithsteve' ),
    'not_found'          => __( 'No Rooms found.', 'sleepwithsteve' ),
    'not_found_in_trash' => __( 'No Rooms found in Trash.', 'sleepwithsteve' )
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'rooms-archive' ),
    'capability_type'    => 'page',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_icon'					 => 'dashicons-admin-network',
    'menu_position'			=> 20,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'custom-fields' )
  );
  register_post_type( 'sws_rooms', $args );
  // TODO: register Packages post-type
  // TODO: register Food & Drink post-type
  // TODO: register Amenities post-type
  // TODO: register Attractions post-type
}

add_action( 'init', 'create_sws_post_types' );

// Create taxonomy for the post type "rooms"
function create_sws_room_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Room Types', 'taxonomy general name' ),
		'singular_name'     => _x( 'Room Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Room Types' ),
		'all_items'         => __( 'All Room Types' ),
		'parent_item'       => __( 'Parent Room Type' ),
		'parent_item_colon' => __( 'Parent Room Type:' ),
		'edit_item'         => __( 'Edit Room Type' ),
		'update_item'       => __( 'Update Room Type' ),
		'add_new_item'      => __( 'Add New Room Type' ),
		'new_item_name'     => __( 'New Room Type Name' ),
		'menu_name'         => __( 'Room Types' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'room-types' ),
	);

	register_taxonomy( 'room_types', array( 'sws_rooms' ), $args );
}

add_action( 'init', 'create_sws_room_taxonomies', 0 );
