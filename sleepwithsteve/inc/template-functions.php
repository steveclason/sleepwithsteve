<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package SleepWithSteve
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function sleepwithsteve_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'sleepwithsteve_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function sleepwithsteve_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'sleepwithsteve_pingback_header' );

/**
 * Add a Booking form.
 * This will almost certainly be replaced with a third-party form, is here for demo purposes.
 */
if ( ! function_exists( 'sleepwithsteve_booking_form' )) {
  function sleepwithsteve_booking_form() {
    echo '<!-- This form is a placeholder, acutual form will depend on booking engine. -->
    <form id="booking-form" class="site-header__booking--form">
      <label for="first-name">First Name</label>
      <input id="first-name" type="text" />
      <label for="last-name">Last Name</label>
      <input id="last-name" type="text" />
      <label for="email">Email</label>
      <input type="email" name="email" id="email" />
      <label for="phone">Phone</label>
      <input id="phone" type="telephone" />
      <label for="room-type">Room Type</label>
      <select id="room-type">
        <option name="suite">Suite</option>
        <option name="suite">Big Room</option>
        <option name="suite">Small Room</option>
        <option name="suite">Tent Out Back</option>
      </select>
      <label for="number-guests">Number of Guests</label>
      <input id="number-guests" type="number" />
      <label for="arrival-date">Arrival Date</label>
      <input id="arrival-date" type="date" />
      <label for="arrival-time">Arrival Time</label>
      <input id="arrival-time" type="time" />
      <label for="departure-date">Departure Date</label>
      <input id="departure-date" type="date" />
      <label for="special-requests">Special Requests</label>
      <textarea placeholder="Please enter your special requests here and we will try to accommodate them."></textarea>
      <input type="submit" value="Book Now" />
    </form>';
  }
}

add_action( 'booking_form', 'sleepwithsteve_booking_form', 10 );
