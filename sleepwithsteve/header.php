<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SleepWithSteve
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sleepwithsteve' ); ?></a>

	<header id="masthead" class="site-header">
    <nav id="site-navigation" class="site-header__menu">
			<div id="menu-button" class="site-header__menu--button" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></div>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
          'menu_class'     => 'menu site-header__menu--list'
				) );
			?>
		</nav><!-- #site-navigation -->

		<div class="site-branding">
      <div class="site-branding__logo">
			  <?php
			  the_custom_logo();
        ?>
      </div>
      <div class="site-branding__titles">
			  <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
        <?php
			  $description = get_bloginfo( 'description', 'display' );
			  if ( $description || is_customize_preview() ) : ?>
				  <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			  <?php
			  endif; ?>
      </div><!-- .site-branding__titles -->
		</div><!-- .site-branding -->

    <div class="site-header__buttons">
		<button class="site-header__telephone">
			<a href="tel:1234567890">Call Now</a>
		</button>

		<div class="site-header__booking">
			<button class="site-header__booking--button">Book Now</button>
      <?php do_action( 'booking_form' ); ?>

		</div>


	</header><!-- #masthead -->

	<div id="content" class="site-content">
